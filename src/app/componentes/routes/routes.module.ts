import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { ContactosComponent } from './contactos/contactos.component';



@NgModule({
  declarations: [
    InicioComponent,
    GaleriaComponent,
    ContactosComponent
  ],
  imports: [
    CommonModule
  ]
})
export class RoutesModule { }
